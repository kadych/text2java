package com.webssk.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.SourceSet

class TextToJavaPlugin implements Plugin<Project> {
    final TEXT_TO_JAVA = 'text2java'

    def void apply(Project project) {
        project.plugins.apply(JavaPlugin)
        project.extensions.create(TEXT_TO_JAVA, TextToJavaPluginExtension)

        project.convention.getPlugin(JavaPluginConvention).sourceSets.all { sourceSet ->
            def buildDir = generateBuildDir(project, sourceSet)
            sourceSet.java.srcDir(buildDir)

            def taskName = sourceSet.getTaskName('generate', 'JavaSource')
            def task = project.tasks.create(taskName, TextToJavaTask)
            task.buildDir = buildDir
            task.sourceSet = sourceSet

            project.tasks.getByName(sourceSet.compileJavaTaskName).dependsOn(task);
        }
    }

    def generateBuildDir(Project project, SourceSet sourceSet) {
        return new File(project.buildDir, "generated-src/${TEXT_TO_JAVA}/${sourceSet.name}")
    }
}

class TextToJavaPluginExtension {
    def packageName = ''
    def codepage = 'utf-8'
    FileCollection source
}

