package com.webssk.gradle

import org.gradle.api.Project

class TextToJava {
    File buildDir = null
    String packageName = ''
    String codepage = 'utf-8'
    Project project = null

    private static String getName(File f) {
        def index = f.name.indexOf('.')
        if (index == -1) {
            return f.name
        }
        return f.name.substring(0, index)
    }

    private static String getExt(File f) {
        def index = f.name.indexOf('.')
        if (index == -1) {
            return ''
        }
        return f.name.substring(index + 1).toLowerCase()
    }

    private static String getClassName(File f) {
        return NameUtils.createClassName(getName(f))
    }

    private static String getOutputName(File f) {
        return getClassName(f) + '.java'
    }

    private static String quoteString(String s) {
        def sb = new StringBuilder()
        s.toCharArray().each { ch ->
            if (ch == '\\') {
                sb.append('\\\\')
            } else if (ch == '\"') {
                sb.append('\\\"')
            } else if ((int) ch < 128) {
                sb.append(ch)
            } else {
                sb.append(String.format("\\u%04X", (int) ch))
            }
        }
        return sb.toString()
    }

    private String relativePath(File f) {
        if (project == null) {
            return f.toString()
        }
        return project.relativePath(f).toString()
    }

    private static String concat(String s1, String s2, String delim) {
        if (!s1.isEmpty() && !s2.isEmpty())
            return s1 + delim + s2
        else if (!s1.isEmpty())
            return s1
        else if (!s2.isEmpty())
            return s2
        return ''
    }

    def File generate(File inputFile) {
        def className = getClassName(inputFile)
        def localPackageName = concat(packageName, getExt(inputFile), '.')
        def outputDir = new File(buildDir, localPackageName.replace('.', '/'))
        outputDir.mkdirs()

        def outputFile = new File(outputDir, getOutputName(inputFile))

        def sb = new PrintWriter(outputFile)
        def indent = '    '

        sb.append('// Class ').append(className).append(' was generated from ').
                append(relativePath(inputFile)).append('\n')
        if (localPackageName != null) {
            sb.append('package ').append(localPackageName).append(';').append('\n')
        }
        sb.append('\n')
        sb.append('public final class ').append(className).append(' {').append('\n')
        sb.append(indent).append('public static final String TEXT = ').append('\n')
        inputFile.eachLine(codepage) { String line ->
            sb.append(indent).append(indent).append('"').append(quoteString(line)).append('\\n" + ').append('\n')
        }
        sb.append(indent).append(indent).append('"";').append('\n\n')
        sb.append(indent).append('@Override').append('\n')
        sb.append(indent).append('public String toString() {').append('\n')
        sb.append(indent).append(indent).append('return TEXT;').append('\n')
        sb.append(indent).append('}').append('\n')
        sb.append('}').append('\n')

        sb.close()

        return outputFile
    }
}
