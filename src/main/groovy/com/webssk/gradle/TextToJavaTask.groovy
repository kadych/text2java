package com.webssk.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskAction

class TextToJavaTask extends DefaultTask {
    def File buildDir
    def SourceSet sourceSet

    @TaskAction
    def text2java() {
        def textToJava = new TextToJava()
        textToJava.packageName = project.text2java.packageName
        textToJava.codepage = project.text2java.codepage
        textToJava.buildDir = buildDir
        textToJava.project = project

        def subPath = getSubPath()
        project.text2java.source.each { File it ->
            if (project.relativePath(it).startsWith(subPath)) {
                File outputFile = textToJava.generate(it);
            }
        }
    }

    def String getSubPath() {
        project.relativePath(project.file("src/${sourceSet.name}")).toString()
    }
}

