package com.webssk.gradle;

public class NameUtils {
    public static String upperFirst(String s) {
        if (s == null || s.isEmpty()) {
            return "";
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String removeUnderlines(String s) {
        if (s == null) {
            return "";
        }
        return s.replace("_", "");
    }

    public static String underlinesToUpper(String s) {
        if (s == null || s.isEmpty()) {
            return "";
        }
        boolean nextUpper = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 0, len = s.length(); i < len; i++) {
            char ch = s.charAt(i);
            if (ch == '_') {
                nextUpper = true;
            } else if (nextUpper) {
                sb.append(String.valueOf(ch).toUpperCase());
                nextUpper = false;
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    public static String createClassName(String s) {
        return upperFirst(underlinesToUpper(s));
    }
}
