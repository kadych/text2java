package com.webssk.gradle;

import org.junit.Test;
import static com.webssk.gradle.NameUtils.*;
import static org.junit.Assert.assertEquals;

public class NameUtilsTest {
    @Test
    public void testUpperFirst() {
        assertEquals("One", upperFirst("one"));
        assertEquals("ONE", upperFirst("ONE"));
        assertEquals("ONE", upperFirst("oNE"));
        assertEquals("O", upperFirst("o"));
        assertEquals("O", upperFirst("O"));
        assertEquals("", upperFirst(""));
        assertEquals("", upperFirst(null));
    }

    @Test
    public void testRemoveUnderlines() {
        assertEquals("onetwothree", removeUnderlines("one_two_three"));
    }

    @Test
    public void testUnderlinesToUpper() {
        assertEquals("one", underlinesToUpper("one"));
        assertEquals("oneTwo", underlinesToUpper("one_two"));
        assertEquals("oneTwoThree", underlinesToUpper("one_two_three"));
    }

    @Test
    public void testCreateClassName() {
        assertEquals("One", createClassName("one"));
        assertEquals("OneTwo", createClassName("one_two"));
        assertEquals("OneTwo", createClassName("One_Two"));
    }
}